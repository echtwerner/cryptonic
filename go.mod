module gitlab.com/echtwerner/cryptonic

go 1.16

require (
	github.com/matryer/is v1.4.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)
